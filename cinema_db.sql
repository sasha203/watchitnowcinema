-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3307
-- Generation Time: May 31, 2017 at 10:26 AM
-- Server version: 5.6.11
-- PHP Version: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cinema_db`
--
CREATE DATABASE IF NOT EXISTS `cinema_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `cinema_db`;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_messages`
--

CREATE TABLE IF NOT EXISTS `tbl_messages` (
  `messageId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `email` varchar(40) NOT NULL,
  `subject` varchar(30) NOT NULL,
  `message` varchar(255) NOT NULL,
  PRIMARY KEY (`messageId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tbl_messages`
--

INSERT INTO `tbl_messages` (`messageId`, `name`, `email`, `subject`, `message`) VALUES
(11, 'john', 'hello@gmail.com', 'Test Message', 'Test message, Helloooooooooooooooooooooooooo.'),
(12, 'test2', 'test2@test2', 'hello', 'hello, test2');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_movies`
--

CREATE TABLE IF NOT EXISTS `tbl_movies` (
  `movieId` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `genre` varchar(20) NOT NULL,
  `length` varchar(20) NOT NULL,
  `comingSoon` char(1) NOT NULL,
  `trailerLink` varchar(80) NOT NULL,
  `screenNum` char(2) NOT NULL,
  `movieImg` varchar(40) NOT NULL,
  PRIMARY KEY (`movieId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tbl_movies`
--

INSERT INTO `tbl_movies` (`movieId`, `title`, `genre`, `length`, `comingSoon`, `trailerLink`, `screenNum`, `movieImg`) VALUES
(1, 'The fate of the furious', 'Action', '2:16', '0', 'https://www.youtube.com/watch?v=JwMKRevYa_M', '1', 'images/TheFateOfTheFurious.jpg'),
(2, 'Beauty and the beast', 'Musical', '2:29', '0', 'https://www.youtube.com/watch?v=SqQvZ_VUtg8', '2', 'images/beautyAndTheBeast.jpeg'),
(3, 'Smurfs', 'Animation', '1:30', '0', 'https://www.youtube.com/watch?v=vu1qZCG6Yo8', '3', 'images/smurfs.jpg'),
(4, 'Logan', 'Action', '2:17', '0', 'https://www.youtube.com/watch?v=gbug3zTm3Ws', '4', 'images/logan.jpg'),
(5, 'Guardians of the galaxy vol.2', 'Adventure', '2:16', '0', 'https://www.youtube.com/watch?v=pr7tDrwQ3t8', '5', 'images/guardiansOfTheGalaxyV2.jpg'),
(6, 'King Arthur: Legend of the Sword', 'Adventure', '2:06', '0', 'https://www.youtube.com/watch?v=jIM4-HLtUM0', '6', 'images/kingArthur.jpg'),
(7, ' Pirates of the Caribbean: Salazar''s Revenge', 'Fantasy', '2:09', '1', 'https://www.youtube.com/watch?v=-6StS7p5TdQ', '', 'images/piratesOfTheCaribbean.jpeg'),
(8, ' Baywatch', 'Action', '1:56', '1', 'https://www.youtube.com/watch?v=eyKOgnaf0BU', '', 'images/baywatch.jpg'),
(9, 'Wonder Woman', 'Fantasy ', '2:21', '1', 'https://www.youtube.com/watch?v=Tgk_63b-Mrw', '', 'images/wonderWoman.jpg'),
(10, 'The Mummy', 'Fantasy', '1:47', '1', 'https://www.youtube.com/watch?v=sCdV3esMr9M', '', 'images/theMummy.jpg'),
(11, 'Transformers: The Last Knight', 'Sci-Fi', '1:54', '1', 'https://www.youtube.com/watch?v=AntcyqJ6brc', '', 'images/transformers.jpg'),
(12, 'Despicable Me 3', 'Animation', '1:36', '1', 'https://www.youtube.com/watch?v=6DBi41reeF0', '', 'images/despicableMe3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstName` varchar(30) NOT NULL,
  `lastName` varchar(30) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `email` varchar(40) NOT NULL,
  `city` varchar(30) NOT NULL,
  `country` varchar(30) NOT NULL,
  `postalCode` varchar(20) NOT NULL,
  `dob` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`userName`),
  UNIQUE KEY `userName_2` (`userName`),
  UNIQUE KEY `userName_3` (`userName`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=130 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `userName`, `password`, `firstName`, `lastName`, `gender`, `email`, `city`, `country`, `postalCode`, `dob`, `mobile`) VALUES
(127, 'sasha203', '69e66274e0cf6d7d9a3bd38f0f80a6bb390f1f7d', 'sasha', 'attard', 'm', 'sashaattard94@gmail.com', 'senglea', 'malta', 'ISL 1162', ' 1/7/1994', '99204749'),
(129, 'test123', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'test', 'test', 'm', 'test@test', 'test', 'test', 'test', ' 13/2/2002', '12312313');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
