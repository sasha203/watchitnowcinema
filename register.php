<?php
	session_start();
	require_once("menu.php");
	require_once("functions.php");
	
	
	//irrid nara namilx l email unique u nara jibqawx jahdmu
	//irrid nara kif ha niranga tad dob li jirisetjaw.
	
	
	if(isset($_POST['submit'])){
		//variables
		$userName = trim($_POST['user']);
		$password = trim($_POST['pass']);
		$firstName = trim($_POST['fName']);
		$lastName = trim($_POST['lName']);
		$gender = $_POST['gender'];
		$email = trim($_POST['email']);
		$city = trim($_POST['city']);
		$country = trim($_POST['country']);
		$postalCode = trim($_POST['postalCode']);
		
		//DOB day-month-year
		$day = 	$_POST['day'];
		$month= $_POST['month'];
		$year = $_POST['year'];
		$dob = " $day/$month/$year";
		
		$mobile = $_POST['mob'];
		
	

		if(empty($userName)||empty($password)||empty($firstName)||empty($lastName)||empty($email)||empty($city)
		||empty($country)||empty($postalCode)||empty($mobile)){
			
?>
		<div class="alert alert-warning alert-dismissable fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Warning!</strong> All fieds must be filled.
		</div>
<?php
		}
		//temporary
		else if(empty($day)||empty($month)||empty($year)){
?>
			<div class="alert alert-warning alert-dismissable fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Warning!</strong> Please fill the DOB field.
			</div>
<?php
		}
		else if(strlen($userName) <6 || strlen($userName) >30){
?>			
			
			<div class="alert alert-warning alert-dismissable fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Warning!</strong> username should be between 6 & 30 characters.
			</div>
<?php	
		}
		else if(strlen($password) <=5){
?>		
			<div class="alert alert-warning alert-dismissable fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Warning!</strong> Password must be greater than 5 characters.
			</div>
<?php
		}
		else if(strlen($firstName) <1 || strlen($firstName) >30){
?>
			<div class="alert alert-warning alert-dismissable fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Warning!</strong> The name field should be between 1 & 30 characters.
			</div>
<?php
		}
		else if(strlen($lastName) <1 || strlen($lastName) >30){
?>
			<div class="alert alert-warning alert-dismissable fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Warning!</strong> The surname field should be between 1 & 30 characters.
			</div>
<?php
		}
		else{
			$conn = connectToDb(); // Will call the connectToDb() function from the connect.php
			
			$insertDetails = " INSERT INTO tbl_user (userName, password, firstName, lastName, gender, email, city, country, postalCode, dob, mobile)
			VALUES('$userName', sha1('$password'), '$firstName', '$lastName' , '$gender', '$email', '$city', '$country', '$postalCode', '$dob', '$mobile')";
			
			
			$result = moveQuery($conn,$insertDetails);
			
?>			
			<div class="alert alert-success alert-dismissable fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Registration Successful!</strong> Congratulations, you are now a member of WatchItNow Cinemas.
			</div>	
<?php		
		}
	}
?>

<!--ghamilt il values fl inputs halli meta ikun em field minhom mhux mimlija ma jiresetjax kollox-->
<form method="post" action="register.php">
	<div class="container-fluid form-group register rc">
		
		<label for="user">Username</label><br/>
		<input type="text" name="user" id="user" class="form-control" value="<?php if(isset($userName)) echo $userName; ?>"><br/>
		
		<label for="pass">Password</label><br/>
		<input type="password" name="pass" id="pass" class="form-control" value="<?php if(isset($password)) echo $password; ?>"><br/>
		
		<label for="name">Name</label><br/>
		<input type="text" name="fName" id="name" class="form-control" value="<?php if(isset($firstName)) echo $firstName; ?>"><br/>
		
		<label for="surname">Surname</label><br/>
		<input type="text" name="lName" id="surname" class="form-control" value="<?php if(isset($lastName)) echo $lastName; ?>"><br/>
		
		<label>Gender</label><br/>		
		<div class="gender">
			<div class="row">
				<div class="col-md-5">
					<div class="col-md-4">
						<input type="radio" name="gender" id="male" value="m" <?php if(isset($_POST['gender']) && $_POST['gender'] == "m") echo"checked";?> checked />
						<label for="male">Male</label>
					</div>
					
					<div class="col-md-4">
						<input type="radio" name="gender" id="female" value="f" <?php if(isset($_POST['gender']) && $_POST['gender'] == "f") echo"checked";?>>
						<label for="female">Female</label>
					</div>
					
					<div class="col-md-4">
						<input type="radio" name="gender" id="other" value="o" <?php if(isset($_POST['gender']) && $_POST['gender'] == "o") echo"checked";?>>
						<label for="other">Other</label>
					</div>
					
				</div>
				<div class="col-md-7"></div>		
			</div>
		</div>
		<br/>
		
		<label for="email">E-mail</label><br/>
		<input type="email" name="email" id="email" class="form-control" value="<?php if(isset($email)) echo $email; ?>"><br/>
		
		<label for="city">City</label><br/>
		<input type="text" name="city" id="city" class="form-control" value="<?php if(isset($city)) echo $city; ?>"><br/>
		
		<!--Nista namila dropdown bil countries-->
		<label for="country">Country</label><br/>
		<input type="text" name="country" id="country" class="form-control" value="<?php if(isset($country)) echo $country; ?>"><br/>
		
		<label for="postalCode">Postal Code</label><br/>
		<input type="text" name="postalCode" id="postalCode" class="form-control" value="<?php if(isset($postalCode)) echo $postalCode; ?>"><br/>
		
		
		<div class="dob"> 
			<label>DOB</label><br/>
			<div class="row">
				<div class="col-md-4">
					<label>Day</label>
					<select name="day" class="form-control dobDropdown" >
						<option value="">Day</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
					</select>
				</div>
				
				<div class="col-md-4">
					<label>Month</label>
					<select name="month" class="form-control dobDropdown" >
						<option value="">Month</option>
						<option value="1">January</option>
						<option value="2">February</option>
						<option value="3">March</option>
						<option value="4">April</option>
						<option value="5">May</option>
						<option value="6">June</option>
						<option value="7">July</option>
						<option value="8">Augest</option>
						<option value="9">September</option>
						<option value="10">October</option>
						<option value="11">November</option>
						<option value="12">December</option>
					</select>
				</div>
				
				<div class="col-md-4">
					<label>Year</label>
					<select name="year" class="form-control dobDropdown">
						<option value="">Year</option>
						<option value="2017">2017</option>
						<option value="2016">2016</option>
						<option value="2015">2015</option>
						<option value="2014">2014</option>
						<option value="2013">2013</option>
						<option value="2012">2012</option>
						<option value="2011">2011</option>
						<option value="2010">2010</option>
					  
						<option value="2009">2009</option>
						<option value="2008">2008</option>
						<option value="2007">2007</option>
						<option value="2006">2006</option>
						<option value="2005">2005</option>
						<option value="2004">2004</option>
						<option value="2003">2003</option>
						<option value="2002">2002</option>
						<option value="2001">2001</option>
						<option value="2000">2000</option>

						<option value="1999">1999</option>
						<option value="1998">1998</option>
						<option value="1997">1997</option>
						<option value="1996">1996</option>
						<option value="1995">1995</option>
						<option value="1994">1994</option>
						<option value="1993">1993</option>
						<option value="1992">1992</option>
						<option value="1991">1991</option>
						<option value="1990">1990</option>

						<option value="1989">1989</option>
						<option value="1988">1988</option>
						<option value="1987">1987</option>
						<option value="1986">1986</option>
						<option value="1985">1985</option>
						<option value="1984">1984</option>
						<option value="1983">1983</option>
						<option value="1982">1982</option>
						<option value="1981">1981</option>
						<option value="1980">1980</option>

						<option value="1979">1979</option>
						<option value="1978">1978</option>
						<option value="1977">1977</option>
						<option value="1976">1976</option>
						<option value="1975">1975</option>
						<option value="1974">1974</option>
						<option value="1973">1973</option>
						<option value="1972">1972</option>
						<option value="1971">1971</option>
						<option value="1970">1970</option>

						<option value="1969">1969</option>
						<option value="1968">1968</option>
						<option value="1967">1967</option>
						<option value="1966">1966</option>
						<option value="1965">1965</option>
						<option value="1964">1964</option>
						<option value="1963">1963</option>
						<option value="1962">1962</option>
						<option value="1961">1961</option>
						<option value="1960">1960</option>

						<option value="1959">1959</option>
						<option value="1958">1958</option>
						<option value="1957">1957</option>
						<option value="1956">1956</option>
						<option value="1955">1955</option>
						<option value="1954">1954</option>
						<option value="1953">1953</option>
						<option value="1952">1952</option>
						<option value="1951">1951</option>
						<option value="1950">1950</option>

						<option value="1949">1949</option>
						<option value="1948">1948</option>
						<option value="1947">1947</option>
						<option value="1946">1946</option>
						<option value="1945">1945</option>
						<option value="1944">1944</option>
						<option value="1943">1943</option>
						<option value="1942">1942</option>
						<option value="1941">1941</option>
						<option value="1940">1940</option>

						<option value="1939">1939</option>
						<option value="1938">1938</option>
						<option value="1937">1937</option>
						<option value="1936">1936</option>
						<option value="1935">1935</option>
						<option value="1934">1934</option>
						<option value="1933">1933</option>
						<option value="1932">1932</option>
						<option value="1931">1931</option> 
						<option value="1930">1930</option>

						<option value="1929">1929</option>
						<option value="1928">1928</option>
						<option value="1927">1927</option>
						<option value="1926">1926</option>
						<option value="1925">1925</option>
						<option value="1924">1924</option>
						<option value="1923">1923</option>
						<option value="1922">1922</option>
						<option value="1921">1921</option>
						<option value="1920">1920</option>

						<option value="1919">1919</option>
						<option value="1918">1918</option>
						<option value="1917">1917</option>
						<option value="1916">1916</option>
						<option value="1915">1915</option>
						<option value="1914">1914</option>
						<option value="1913">1913</option>
						<option value="1912">1912</option>
						<option value="1911">1911</option>
						<option value="1910">1910</option>

						<option value="1909">1909</option>
						<option value="1908">1908</option>
					</select>
				</div>
			</div>
		</div><br/>
		
		<label for="mob">Mobile</label><br/>
		<input type="number" name="mob" id="mob" class="form-control" value="<?php if(isset($mobile)) echo $mobile; ?>"><br/>
		
		<button name="submit" class="btn btn-primary hidden-xs">Register</button>
		<!-- Mobile view submit button-->
		<button name="submit" class="btn btn-primary mobSubmit visible-xs">Register</button>
			
	</div>
</form>
<?php
	require_once("footer.php");
?>