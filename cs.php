<?php
	session_start();
	require_once("menu.php");
	require_once("functions.php");
	
	
	$conn = connectToDb();	
	require_once("movies.php");
?>
<div class="container rc iSize">
		<div class="row"> <!-- Row1 -->
			<div class="col-md-4">
				<img class="img-rounded" src="<?php echo $poc['movieImg'];?>" alt="pirates_of_the_caribbean"  />
				<div class="mDescription">
					<h4><?php echo $poc['title'];?></h4>
					<h5>Duration: <?php echo $poc['length'];?> | Genre: <?php echo $poc['genre'];?> </h5>
					<a class="btn btn-primary" target="_blank" href="<?php echo $poc['trailerLink'];?>">Watch Trailer</a>	
				</div>
			</div>
			
			<div class="col-md-4">
				<img class="img-rounded" src="<?php echo $bw['movieImg'];?>" alt="baywatch"  />
				<div class="mDescription">
					<h4><?php echo $bw['title'];?></h4>
					<h5>Duration: <?php echo $bw['length'];?> | Genre: <?php echo $bw['genre'];?> </h5>
					<a class="btn btn-primary" target="_blank" href="<?php echo $bw['trailerLink'];?>">Watch Trailer</a>	
				</div>
			</div>
			
			<div class="col-md-4">
				<img class="img-rounded" src="<?php echo $ww['movieImg'];?>" alt="wonder_Woman"  />
				<div class="mDescription">
					<h4><?php echo $ww['title'];?></h4>
					<h5>Duration: <?php echo $ww['length'];?> | Genre: <?php echo $ww['genre'];?> </h5>
					<a class="btn btn-primary" target="_blank" href="<?php echo $ww['trailerLink'];?>">Watch Trailer</a>	
				</div>
			</div>
		</div>
		
		<div class="row"> <!-- Row2 -->	
			<div class="col-md-4">
				<img class="img-rounded" src="<?php echo $mummy['movieImg'];?>" alt="the_mummy"  />
				<div class="mDescription">
					<h4><?php echo $mummy['title'];?></h4>
					<h5>Duration: <?php echo $mummy['length'];?> | Genre: <?php echo $mummy['genre'];?> </h5>
					<a class="btn btn-primary" target="_blank" href="<?php echo $mummy['trailerLink'];?>">Watch Trailer</a>	
				</div>
			</div>
			
			<div class="col-md-4">
				<img class="img-rounded" src="<?php echo $trans['movieImg'];?>" alt="transformers"  />
				<div class="mDescription">
					<h4><?php echo $trans['title'];?></h4>
					<h5>Duration: <?php echo $trans['length'];?> | Genre: <?php echo $trans['genre'];?> </h5>
					<a class="btn btn-primary" target="_blank" href="<?php echo $trans['trailerLink'];?>">Watch Trailer</a>	
				</div>
			</div>
			
			<div class="col-md-4">
				<img class="img-rounded" src="<?php echo $d3['movieImg'];?>" alt="despicable_me3"  />
				<div class="mDescription">
					<h4><?php echo $d3['title'];?></h4>
					<h5>Duration: <?php echo $d3['length'];?> | Genre: <?php echo $d3['genre'];?> </h5>
					<a class="btn btn-primary" target="_blank" href="<?php echo $d3['trailerLink'];?>">Watch Trailer</a>	
				</div>
			</div>
		</div>
		
</div>
	
<?php
	require_once("footer.php");
?>