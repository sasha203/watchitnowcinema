<?php
	session_start();
	require_once("menu.php");
	require_once("functions.php");
	
	
	if(isset($_POST['submit'])){
		//Contact form Variables
		$fName = trim($_POST['name']);
		$email = trim($_POST['email']);
		$subject = trim($_POST['subject']);
		$message = trim($_POST['message']);
	
		if(empty($fName)||empty($email)||empty($subject)||empty($message)){
?>
			<div class="alert alert-warning alert-dismissable fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Warning!</strong>Please fill all fields.
			</div>
<?php
		}else{
			$conn = connectToDb();
			
			$insertMessage = " INSERT INTO tbl_messages (name, email, subject, message)
			VALUES ('$fName', '$email', '$subject', '$message') ";
			
			$result = moveQuery($conn,$insertMessage);
			
?>
			<div class="alert alert-success alert-dismissable fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Message Sent!</strong>
			</div>	
<?php		
		}
	}
?>
<div class="container-fluid contactForm rc">
	<form method="post" action="contact.php" class="form-group" >
		<label>Name</label><br/>
		<input type="text" name="name" class="form-control"/></br>
		<label>Email</label><br/>
		<input type="email" name="email" class="form-control"/></br>
		<label>Subject</label><br/>
		<input type="text" name="subject" class="form-control"/></br>
		<label>Message</label><br/>
		<textarea rows="8" class="form-control" name="message"></textarea><br/>
		<button class="btn btn-primary" name="submit">Submit</button>
	</form>
</div>	
<?php	
	require_once("footer.php");
?>