var menudata={children:[
{text:"Main Page",url:"index.html"},
{text:"Files",url:"files.html",children:[
{text:"File List",url:"files.html"},
{text:"Globals",url:"globals.html",children:[
{text:"All",url:"globals.html",children:[
{text:"$",url:"globals.html#index_0x24"},
{text:"c",url:"globals.html#index_c"},
{text:"i",url:"globals.html#index_i"},
{text:"m",url:"globals.html#index_m"},
{text:"w",url:"globals.html#index_w"}]},
{text:"Functions",url:"globals_func.html"},
{text:"Variables",url:"globals_vars.html",children:[
{text:"$",url:"globals_vars.html#index_0x24"},
{text:"i",url:"globals_vars.html#index_i"},
{text:"w",url:"globals_vars.html#index_w"}]}]}]}]}
