<?php
	session_start();
	require_once("functions.php");
	
	if(isset($_POST['loginSubmit'])){
		//variables
		$userName = trim($_POST['user']);
		$password = trim($_POST['pass']);
		
		if(empty($userName)||empty($password)){
?>
			<div class="alert alert-warning alert-dismissable fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Warning!</strong> Make sure that both Username and Password are not empty.
			</div>
<?php	
		}
		else{
			$conn = connectToDb(); // Will call the connectToDb() function from the connect.php
			
			$accessQuery = " SELECT COUNT(*) FROM tbl_user
			WHERE userName = '$userName' AND password = sha1('$password') ";
			
			$result = moveQuery($conn,$accessQuery);
			
			
			//jekk l user ul pass jaqblu ma 1 fid database $counter jigi assigned b value ta 1.
			$row = mysqli_fetch_row($result);
			$counter = $row[0];
			
			//jekk il user jezisti fi db jicreatja session.
			if($counter > 0){
				//sessions
				$_SESSION['user'] = $userName;
				$_SESSION['time'] = time(); //just in case nigi biex nuri kemm ijlu logged.
				header("Location: home.php");
			}
		}
	}
				
?>				
<?php
	
	require_once("menu.php");
	
?>
<div class="container-fluid loginForm rc">
<form method="post" action="login.php">
	
		<label>Username</label><br/>
		<input type="text" name="user" class="form-control" /><br/>
		
		<label>Password</label><br/>
		<input type="password" name="pass" class="form-control"/><br/>
		<input type="submit" name="loginSubmit" class="btn btn-primary login" value="Login">
	
		
		<!--button originally made for testing may leave it here. -->
		<!--<button name="logout" class="btn btn-danger">Logout</button>-->
	
</form>
</div>
<?php
			if(isset($_SESSION['user'])){
?>
				<br/>
				<div class="alert alert-info alert-dismissable fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Info!</strong> You are logged in as <?php echo "$_SESSION[user]"?>
				</div>
<?php
			}
			else if(isset($userName) && isset($password)){
?>
				<div class="alert alert-warning alert-dismissable fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Warning!</strong> Incorrect Username or Password.
				</div>
<?php	
			}
			
	
	require_once("footer.php");
?>