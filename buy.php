<?php
	session_start();
	require_once("menu.php");
	require_once("functions.php");
	
	$conn = connectToDb();	
	require_once("movies.php");
	
	$getImgQuery = "SELECT * FROM tbl_movies";
	$result = moveQuery($conn,$getImgQuery);
?>
	<div class="container rc buyMovie">
		<h3>Please choose a movie</h3>
		<select class="form-control" id="movies">
		<?php
			while($row = mysqli_fetch_assoc($result)){ //This loop will set the movie IDs as values, the data-src will be set to the image path and the movie titles will appear as options in the combo box.
		?>
				<option value="<?php echo"$row[movieId]"?>"  data-src="<?php echo"$row[movieImg]"?>"><?php echo"$row[title]"?></option>
		<?php
			}
		?>
		</select>
		<br/><br/>
		
			<?php require_once("movies.php");?><!-- used $fast8 from movies.php to get the image of the movie's row.-->
			<img id="imageToSwap" src="<?php echo $fast8['movieImg']?>" width="40%" /> <!-- Niranga naqa kif joqodu uwekk-->
			<div class="movieInfo">
				<h4>Price: <span>&euro;6</span></h4>
				<h4 id="quantityTxt">Quantity</h4>
				
				<select id="quantity" class="form-control" onchange="calcQuantity(value)">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
				</select>
				
				<h4 id="total"></h4>
				
				<button class="btn btn-primary" onclick="purchase()">Purchase</button>
			</div>	
	</div>
		
<script src="js/buy.js"></script>		
<?php
	require_once("footer.php");
?>