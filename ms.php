<?php
	session_start();
	require_once("menu.php");
	require_once("functions.php");
	
	
	if(isset($_SESSION['user'])){
?>				
		<!-- When the user is logged in the buy ticket buttons are enabled. -->
		<script>
			var elementss = document.getElementsByClassName('buyButt');

			function enableBuyButt(){
				for (var i = 0 ; i < elementss.length; i++ ){
					elementss[i].classList.remove('disabled');
				}
			}


			window.onload = function(){
				if(true){
					enableBuyButt();
				}
			}
		</script>
<?php
	}
	$conn = connectToDb();	
	require_once("movies.php");
?>


	<div class="container rc iSize">
		<div class="row"> <!-- Row1 -->
			<div class="col-md-4">
				<img class="img-rounded" src="<?php echo $fast8['movieImg'];?>" alt="fast8"  />
				<div class="mDescription">
					<h4><?php echo $fast8['title'];?></h4>
					<h5>Screen <?php echo $fast8['screenNum'];?> | Duration: <?php echo $fast8['length'];?> | Genre: <?php echo $fast8['genre'];?> </h5>
					<a class="btn btn-primary disabled buyButt" href="buy.php">Buy Ticket!</a><!-- Need to link to another page href="" -->
					<a class="btn btn-primary" target="_blank" href="<?php echo $fast8['trailerLink'];?>">Watch Trailer</a>	
				</div>
			</div>
			
			
			<div class="col-md-4">
				<img class="img-rounded" src="<?php echo $bab['movieImg'];?>" alt="beauty&TheBeast"  />
				<div class="mDescription">
					<h4><?php echo $bab['title'];?></h4>
					<h5>Screen <?php echo $bab['screenNum'];?> | Duration: <?php echo $bab['length'];?> | Genre: <?php echo $bab['genre'];?></h5>
					<a class="btn btn-primary disabled buyButt" href="buy.php">Buy Ticket!</a><!-- Need to link to another page href="" -->
					<a class="btn btn-primary" target="_blank" href="<?php echo $bab['trailerLink'];?>">Watch Trailer</a>	
				</div>
			</div>
			
			<div class="col-md-4">
				<img class="img-rounded" src="<?php echo $smurfs['movieImg'];?>"  alt="smurfs"  />
				<div class="mDescription">
					<h4><?php echo $smurfs['title'];?></h4>
					<h5>Screen <?php echo $smurfs['screenNum'];?> | Duration: <?php echo $smurfs['length'];?> | Genre: <?php echo $smurfs['genre'];?> </h5>
					<a class="btn btn-primary disabled buyButt" href="buy.php">Buy Ticket!</a><!-- Need to link to another page href="" -->
					<a class="btn btn-primary" target="_blank" href="<?php echo $smurfs['trailerLink'];?>">Watch Trailer</a>	
				</div>
			</div>
		</div>
		
		
		<div class="row"><!-- Row2 -->
			<div class="col-md-4">
				<img class="img-rounded" src="<?php echo $logan['movieImg'];?>" alt="logan"  />
				<div class="mDescription">
					<h4><?php echo $logan['title'];?></h4>
					<h5>Screen <?php echo $logan['screenNum'];?> | Duration: <?php echo $logan['length'];?> | Genre: <?php echo $logan['genre'];?> </h5>
					<a class="btn btn-primary disabled buyButt" href="buy.php">Buy Ticket!</a><!-- Need to link to another page href="" -->
					<a class="btn btn-primary" target="_blank" href="<?php echo $logan['trailerLink'];?>">Watch Trailer</a>	
				</div>
				
			</div>
			
			<div class="col-md-4">
				<img class="img-rounded" src="<?php echo $gotgv2['movieImg'];?>" alt="guardians_of_the_galaxy_v2"  />
				<div class="mDescription">
					<h4><?php echo $gotgv2['title'];?></h4>
					<h5>Screen <?php echo $gotgv2['screenNum'];?> | Duration: <?php echo $gotgv2['length'];?> | Genre: <?php echo $gotgv2['genre'];?> </h5>
					<a class="btn btn-primary disabled buyButt" href="buy.php">Buy Ticket!</a><!-- Need to link to another page href="" -->
					<a class="btn btn-primary" target="_blank" href="<?php echo $gotgv2['trailerLink'];?>">Watch Trailer</a>	
				</div>
			</div>
			
			<div class="col-md-4">
				<img class="img-rounded" src="<?php echo $kArthur['movieImg'];?>"  alt="king_arthur_legend_of_the_sword"  />
				<div class="mDescription">
					<h4><?php echo $kArthur['title'];?></h4>
					<h5>Screen <?php echo $kArthur['screenNum'];?> | Duration: <?php echo $kArthur['length'];?> | Genre: <?php echo $kArthur['genre'];?> </h5>
					<a class="btn btn-primary disabled buyButt" href="buy.php">Buy Ticket!</a><!-- Need to link to another page href="" -->
					<a class="btn btn-primary" target="_blank" href="<?php echo $kArthur['trailerLink'];?>">Watch Trailer</a>	
				</div>
			</div>
		</div>
		
	</div>
	
<?php
	require_once("footer.php");
?>