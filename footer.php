		<footer class="container-fluid footerCanvas">
			<!-- Desktop Layout-->
			<div class="desktopFooter hidden-xs">
				<div class="bottomLinks">
					<ul class="nav nav-pills">
					  <li role="presentation"><a href="home.php">Home</a></li>
					  <li role="presentation"><a href="ms.php">Movie Schedule</a></li>
					  <li role="presentation"><a href="cs.php">Coming Soon</a></li>
					  <li role="presentation"><a href="contact.php">Contact Us</a></li>
					  <li role="presentation"><a href="addOff.php">Additional Offers</a></li>
					</ul>
				</div>
				
				<div class="info"> <!--Contact information-->
					<!--Please note, All information is invalid.-->
					<address>
						<abbr title="Address"><span class="glyphicon glyphicon-map-marker"></span></abbr>
						<strong>WatchItNow Cinema,</strong> </br>
						113,trueStreet, <br/>
						Sliema SLM 3110 <br/>
					</address>
					<address>
						<abbr title="Telephone Number"><span class="glyphicon glyphicon-earphone"></span></abbr>
						(+365)27111111 Or (+365)21111111 <br/>
					</address>
					<address>
						<abbr title="E-mail"><span class="glyphicon glyphicon-envelope"></span></abbr>
						<a href="mailto:WatchItNow@gmail.com">WatchItNow@gmail.com</a>
					</address>				
				</div>
			</div>
			
			<!--Mobile Layout-->
			<div class="mobFooter visible-xs">
				<div class="bottomLinks" >
					<ul class="nav nav-pills nav-stacked">
					  <li role="presentation"><a href="home.php">Home</a></li>
					  <li role="presentation"><a href="ms.php">Movie Schedule</a></li>
					  <li role="presentation"><a href="cs.php">Coming Soon</a></li>
					  <li role="presentation"><a href="contact.php">Contact Us</a></li>
					  <li role="presentation"><a href="addOff.php">Additional Offers</a></li>
					</ul>
				</div>
				<div class="info"> <!--Contact information-->
					<!--Please note, All information is invalid.-->
					<address>
						<abbr title="Address"><span class="glyphicon glyphicon-map-marker"></span></abbr>
						<strong>WatchItNow Cinema,</strong> </br>
						113,trueStreet, <br/>
						Sliema SLM 3110 <br/>
					</address>
					<address>
						<abbr title="Telephone Number"><span class="glyphicon glyphicon-earphone"></span></abbr>
						(+365)27111111 Or (+365)21111111 <br/>
					</address>
					<address>
						<abbr title="E-mail"><span class="glyphicon glyphicon-envelope"></span></abbr>
						<a href="mailto:WatchItNow@gmail.com">WatchItNow@gmail.com</a>
					</address>				
				</div>
			</div>
			
			
		</footer>

		<!-- Google JQuery CDN-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
		
		<!-- Bootstrap js-->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" 
		integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		
		<script src="js/menu.js"></script>
		
	</body>
</html>