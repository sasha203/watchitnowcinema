<!DOCTYPE html>
<html lang="en">
	<head>
		<title>SD Project</title> <!-- irrid nara kif ha namel title jinbidel u nara kif ha namel l active tal bootstrap jinbidel kull page differenti -->
		
		<!-- Bootstrap css and Theme css -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" 
		integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" 
		integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		
		<link rel="stylesheet" type="text/css" href="css/main.css" />

<?php
			if(isset($_SESSION['user'])){
?>
				<!--when user logs in the log in button disappears while logout appears.-->
				<style>
					.login{
						display:none;
					}
					.logout{
						display:inline;
					}
					.welcMsg{
						display:inline;
						font-weight: bold;
					}
					
					.loginButt{
						width:0%;
					}
				</style>
<?php
	}
?>	
	</head>
	
	<body>	
		<div class="accountNav">
			<div class="logo">
				<img src="images/logo.png"/>
			</div>

			<div class="rightSect">	
			
			
				<div class="hidden-xs regButt">
					<form  action="register.php">
						<input type="submit" class="btn btn-default" value="Register" /> <!--id="reg"-->	
					</form>
				</div>
			
			
				<?php if(isset($_SESSION['user'])){ ?>
				<div class="hidden-xs logoutButt">
					<form method="POST" action="logout.php">
						<span class="welcMsg">Welcome, <?php echo "$_SESSION[user]"."&nbsp;"; ?></span>
						<input type="submit" name="logout" class="btn btn-danger logout" Value="logOut"/>
					</form>
				</div>
				<?php
				}
				else{
					?>
					<div class="hidden-xs loginButt">
						<form method="POST" action="login.php">
							<input type="submit" class="btn btn-primary glyphicon glyphicon-log-in login" value="Login"/>
						</form>
					</div>
				<?php
				}
				?>
				
				
				
			</div>
		</div>
		<br/>	
		<!-- For better mobile display-->
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<!-- navbar-header is the header of the mobile display. -->
				<div class="navbar-header visible-xs"> <!-- Visible-xs biex tidher meta jkun mobile biss-->
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="home.php">Menu</a>
				</div>

				<!--Nav links and other content for toggling (login,register & search bar are appended in mobile view.) -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<a href="register.php" class="btn btn-default glyphicon glyphicon-pencil mobReg visible-xs " role="button"> Register</a>
						<li><a href="home.php">Home</a></li>
						<li><a href="ms.php">Movie Schedule</a></li>
						<li><a href="cs.php">Coming Soon</a></li>
						<li><a href="contact.php">Contact Us</a></li>
						<li><a href="addOff.php">Additional Offers</a></li>
					</ul>
					
					<!-- Desktop Search bar-->
					<div class="pull-right searchBar hidden-xs">
						<form class="navbar-form" role="search">
							<div class="input-group">
								<input type="text" class="form-control" placeholder="Search">
								<div class="input-group-btn">
									<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
								</div>
							</div>
						</form>
					</div>
				
			</div>
		</nav>
		




