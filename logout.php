<?php
	session_start();
	//if the buttons with logout name are clicked the session will end.
	if(isset($_POST['logout'])){
		unset($_SESSION['user']);
		unset($_SESSION['time']);
		session_destroy();
	}
	
	header("Location:home.php");
?>