<?php
	session_start();
	require_once("menu.php");
	require_once("functions.php");
	$conn = connectToDb();	
	
	//Movies for carousel.
	require_once("movies.php");
?>

<div class="container rc">	
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
		</ol>
		
		<!-- Wrapper for slides -->
		<div class="carousel-inner">
			<div class="item active">
				<img src="<?php echo $fast8['movieImg'];?>" width="100%" alt="fast8"  />
			</div>
			
			<div class="item">
				<img src="<?php echo $gotgv2['movieImg'];?>" width="100%" alt=""/>
			</div>

			<div class="item">
				<img src="<?php echo $kArthur['movieImg'];?>" width="100%" alt="king_arthur" />
			</div>
		</div>
		
		<!-- Left and right controls -->
		<a class="left carousel-control" href="#myCarousel" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
			<span class="sr-only">Previous</span>
		</a>
		
		<a class="right carousel-control" href="#myCarousel" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
			<span class="sr-only">Next</span>
		</a>
		
	</div>
	

</div>






<?php
	require_once("footer.php");
?>